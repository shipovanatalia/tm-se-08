package ru.shipova.tm;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.command.project.ProjectClearCommand;
import ru.shipova.tm.command.project.ProjectCreateCommand;
import ru.shipova.tm.command.project.ProjectListCommand;
import ru.shipova.tm.command.project.ProjectRemoveCommand;
import ru.shipova.tm.command.system.AboutCommand;
import ru.shipova.tm.command.system.ExitCommand;
import ru.shipova.tm.command.system.HelpCommand;
import ru.shipova.tm.command.task.*;
import ru.shipova.tm.command.user.*;

public final class Application {

    private static final Class[] CLASSES = {
            AboutCommand.class, ExitCommand.class, HelpCommand.class,

            UserLoginCommand.class, UserLogoutCommand.class, UserProfileCommand.class,
            UserRegistryCommand.class, UserSetPasswordCommand.class, UserUpdateCommand.class,

            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectListCommand.class, ProjectRemoveCommand.class,

            TaskClearCommand.class, TaskCreateCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class, TaskShowCommand.class
    };

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }
}
