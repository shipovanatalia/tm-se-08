package ru.shipova.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public final class TaskService implements ITaskService {
    private final  ITaskRepository taskRepository;
    private final  IProjectRepository projectRepository;

    @Nullable
    @Override
    public List<String> showAllTasksOfProject(final @Nullable String projectName) throws ProjectDoesNotExistException {
        if (projectName == null || projectName.isEmpty()) return null;

        final @Nullable String projectId = projectRepository.getProjectIdByName(projectName);
        if (projectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        return taskRepository.showAllTasksOfProject(projectId);
    }

    @Nullable
    @Override
    public List<Task> getListTask(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String taskName, final @Nullable String projectName) throws ProjectDoesNotExistException {
        if (userId == null || userId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        if (projectName == null || projectName.isEmpty()) return;

        final @Nullable String projectId = projectRepository.getProjectIdByName(projectName);
        if (projectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        final @NotNull String taskId = UUID.randomUUID().toString();
        taskRepository.persist(new Task(taskId, taskName, projectId, userId));
    }

    @Override
    public void clear(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByUserId(userId);
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable String taskName) {
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        final @Nullable String taskId = taskRepository.getTaskIdByName(taskName);
        final @Nullable Task task = taskRepository.findOne(taskId);

        if (task == null) return;
        if (!userId.equals(task.getUserId())) return;

        taskRepository.remove(taskId);
    }
}
