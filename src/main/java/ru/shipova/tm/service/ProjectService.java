package ru.shipova.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public final class ProjectService implements IProjectService {
    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    @Nullable
    @Override
    public List<Project> getListProject(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String projectName) {
        if (userId == null || userId.isEmpty()) return;
        if (projectName == null || projectName.isEmpty()) return;

        final @NotNull String projectId = UUID.randomUUID().toString();
        projectRepository.persist(new Project(projectId, userId, projectName));
    }

    @Override
    public void clear(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        final @Nullable String projectId = projectRepository.getProjectIdByName(projectName);

        final @Nullable Project project = projectRepository.findOne(projectId);
        if (project == null) return;
        if (!userId.equals(project.getUserId())) return;

        projectRepository.remove(projectId);
        taskRepository.removeAllTasksOfProject(projectId);
    }
}
