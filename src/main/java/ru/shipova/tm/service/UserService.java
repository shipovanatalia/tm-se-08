package ru.shipova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.util.PasswordHashUtil;

import java.util.UUID;

@Getter
@Setter
public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    @Nullable
    private User currentUser;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
        currentUser = null;
    }

    @Nullable
    @Override
    public User authorize(final @Nullable String login, final @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean checkDataAccess(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;

        final @Nullable User user = findByLogin(login);
        if (user == null) return false;

        final @NotNull String passwordHash = PasswordHashUtil.md5(password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public RoleType getRoleType(final @Nullable String role){
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }

    @Override
    public void registryUser(final @Nullable String login, final @Nullable String password, final @Nullable String role) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;

        final @NotNull String passwordHash = PasswordHashUtil.md5(password);
        final @NotNull String userId = UUID.randomUUID().toString();

        final @Nullable RoleType roleType = getRoleType(role);
        userRepository.persist(new User(userId, login, passwordHash, roleType));
    }

    @Override
    public void updateUser(final @Nullable String login, final @Nullable String role){
        if (role == null || role.isEmpty()) return;
        if (login == null || login.isEmpty()) return;

        final @Nullable RoleType roleType = getRoleType(role);
        userRepository.updateUser(login, roleType);
    }

    @Override
    public void setNewPassword(final @Nullable String login, final @Nullable String password) {
        if (password == null || password.isEmpty()) return;
        if (login == null || login.isEmpty()) return;

        final @NotNull String passwordHash = PasswordHashUtil.md5(password);
        userRepository.setNewPassword(login, passwordHash);
    }
}
