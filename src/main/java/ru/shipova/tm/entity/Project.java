package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String userId;

    @Nullable
    private String description;

    @Nullable
    private Date dateOfBegin;

    @Nullable
    private Date dateOfEnd;

    public Project(@NotNull final String id, @Nullable final String userId, @Nullable final String name) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }
}
