package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateOfBegin;

    @Nullable
    private Date dateOfEnd;

    @Nullable
    private String userId;

    public Task(@NotNull final String id, @Nullable final String name, @Nullable final String projectId, @Nullable final String userId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.userId = userId;
    }
}
