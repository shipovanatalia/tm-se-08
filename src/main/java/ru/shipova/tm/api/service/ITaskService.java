package ru.shipova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

import java.util.List;

public interface ITaskService {

    @Nullable List<String> showAllTasksOfProject(@Nullable final String projectName) throws ProjectDoesNotExistException;

    @Nullable List<Task> getListTask(@Nullable final String userId);

    void create(@Nullable final String userId, @Nullable final String taskName, @Nullable final String projectName) throws ProjectDoesNotExistException;

    void clear(@Nullable final String userId);

    void remove(@Nullable final String userId, @Nullable final String taskName);
}
