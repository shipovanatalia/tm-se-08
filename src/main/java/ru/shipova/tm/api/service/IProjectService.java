package ru.shipova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    @Nullable List<Project> getListProject(@Nullable final String userId);

    void create(@Nullable final String userId, @Nullable final String projectName);

    void clear(@Nullable final String userId);

    void remove(@Nullable final String userId, @Nullable final String projectName);
}
