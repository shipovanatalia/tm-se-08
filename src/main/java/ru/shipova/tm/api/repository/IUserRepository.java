package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;

public interface IUserRepository {
    @Nullable User findByLogin(final String login);

    void persist(@Nullable final User user);

    void setNewPassword(@Nullable final String login,@Nullable final String passwordHash);

    void updateUser(@Nullable final String login, @Nullable final RoleType roleType);
}
