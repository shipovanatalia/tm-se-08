package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
     @Nullable Project findOne(@Nullable final String projectId);

     @Nullable List<Project> findAll();

     @Nullable List<Project> findAllByUserId(@Nullable final String userId);

     @Nullable String getProjectIdByName(@Nullable final String projectName);

     void persist(@Nullable final Project project);

     void merge(@Nullable final Project project);

     void remove(@Nullable final String projectId);

     void remove(@Nullable final Project project);

     void removeAllByUserId(@Nullable final String userId);

     void update(@Nullable final Project project);
}
