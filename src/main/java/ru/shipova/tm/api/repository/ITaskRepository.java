package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    @Nullable Task findOne(@Nullable final String taskId);

    @Nullable List<Task> findAll();

    @Nullable List<Task> findAllByUserId(@Nullable final String userId);

    @Nullable String getTaskIdByName(@Nullable final String taskName);

    /**
     * Метод вставляет новый объект, если его не было. Данные объекта не обновляет и не перезатирает.
     *
     * @param task - объект, который необходимо вставить
     */

    void persist(@Nullable final Task task);

    /**
     * Метод вставляет новый объект, если его не было.
     * Если объект был, он его обновляет.
     *
     * @param task - объект, который необходимо вставить
     */
    void merge(@Nullable final Task task);

    void remove(@Nullable final String taskId);

    void remove(@Nullable final Task task);

    void removeAllByUserId(@Nullable final String userId);

    /**
     * Метод находит объект с идентичным id и обновляет все его поля на поля объекта task.
     *
     * @param task - объект с новыми данными.
     */
    void update(@Nullable final Task task);

    @Nullable List<String> showAllTasksOfProject(@Nullable final String projectId);

    void removeAllTasksOfProject(@Nullable final String projectId);
}
