package ru.shipova.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.entity.Task;

import java.util.*;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllByUserId(final String userId) {
        final List<Task> tasks = new ArrayList<>();

        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (userId.equals(entry.getValue().getUserId())){
                tasks.add(entry.getValue());
            }
        }
        return tasks;
    }

    @Nullable
    @Override
    public String getTaskIdByName(final String taskName){
        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @NotNull
    @Override
    public List<String> showAllTasksOfProject(final String projectId) {
        final List<String> listOfTasks = new ArrayList<>();

        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (projectId.equals(entry.getValue().getProjectId())) {
                listOfTasks.add(entry.getValue().getName());
            }
        }
        return listOfTasks;
    }

    @Override
    public void removeAllByUserId(final String userId) {
        final List<Task> tasksToRemove = findAllByUserId(userId);
        for (Task task : tasksToRemove) {
            for (Map.Entry<String, Task> entry : getMap().entrySet()) {
                if (task.getId().equals(entry.getKey())) {
                    remove(task);
                }
            }
        }
    }

    @Override
    public void update(final Task task) {
        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (task.getId().equals(entry.getKey())) {
                entry.getValue().setName(task.getName());
                entry.getValue().setDescription(task.getDescription());
                entry.getValue().setProjectId(task.getProjectId());
                entry.getValue().setDateOfBegin(task.getDateOfBegin());
                entry.getValue().setDateOfEnd(task.getDateOfEnd());
            }
        }
    }

    @Override
    public void removeAllTasksOfProject(final String projectId) {
        final Iterator<Map.Entry<String, Task>> iterator = getMap().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (projectId.equals(entry.getValue().getProjectId())) {
                iterator.remove();
            }
        }
    }
}
