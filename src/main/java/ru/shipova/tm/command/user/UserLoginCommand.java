package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Login in task manager.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        final IUserService userService = serviceLocator.getIUserService();
        @Nullable final User user = userService.authorize(login, password);
        if (user == null) {
            System.out.println("WRONG LOGIN OR PASSWORD");
            execute();
        }
        userService.setCurrentUser(user);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
