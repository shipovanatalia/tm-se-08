package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        final @NotNull IProjectService projectService = serviceLocator.getIProjectService();
        final @NotNull IUserService userService = serviceLocator.getIUserService();
        final @Nullable User currentUser = userService.getCurrentUser();
        final @Nullable String userId = currentUser != null ? currentUser.getId() : null;
        projectService.clear(userId);
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
