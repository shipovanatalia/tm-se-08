package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.User;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final IUserService userService = serviceLocator.getIUserService();
        @Nullable final User currentUser = userService.getCurrentUser();
        @Nullable final String userId = currentUser != null ? currentUser.getId() : null;

        int index = 1;
        for (@Nullable Project project : projectService.getListProject(userId)) {
            System.out.println(index++ + ". " + (project != null ? project.getName() : null));
        }
        System.out.println();
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
