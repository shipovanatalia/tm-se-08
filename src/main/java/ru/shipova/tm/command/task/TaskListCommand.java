package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final IUserService userService = serviceLocator.getIUserService();
        @Nullable final User currentUser = userService.getCurrentUser();
        @Nullable final String userId = currentUser != null ? currentUser.getId() : null;

        int index = 1;
        for (@Nullable Task task : taskService.getListTask(userId)) {
            System.out.println(index++ + ". " + (task != null ? task.getName() : null));
        }
        System.out.println();
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
