package ru.shipova.tm.command;

import lombok.Setter;
import ru.shipova.tm.api.IServiceLocator;

@Setter
public abstract class AbstractCommand {
    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean needAuthorize();

    public boolean isOnlyAdminCommand(){
        return false;
    }
}
