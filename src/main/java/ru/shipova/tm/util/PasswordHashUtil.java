package ru.shipova.tm.util;

import org.jetbrains.annotations.Nullable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class PasswordHashUtil {
    @Nullable
    public static String md5(final @Nullable String password) {
        if (password == null || password.isEmpty()) return null;
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(password.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        // получаем MD5-хеш строки без лидирующих нулей
        final String s = new BigInteger(1, md.digest()).toString(16);
        final StringBuilder sb = new StringBuilder(32);
        // дополняем нулями до 32 символов, в случае необходимости
        for (int i = 0, count = 32 - s.length(); i < count; i++) {
            sb.append("0");
        }
        // возвращаем MD5-хеш
        return sb.append(s).toString();
    }
}
